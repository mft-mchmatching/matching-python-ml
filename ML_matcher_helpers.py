import sys
import time
import numpy as np
from sklearn.neural_network import MLPRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import GradientBoostingRegressor
import matplotlib.pyplot as plt
import numpy as np
import matplotlib

from sklearn.metrics import mean_squared_error
from supervised.automl import AutoML # mljar-supervised

def load_data(nPions, nMCHTracks="1000", nMuons="20"):
# wrapper por load_data_from_file for files such as data_1000MCHTraks_20Mu_1200PiPerEvent.py
# inputs: 
#  - nPions: number of backgroundPions
#  - nMCHTracks: number
    datafile = "data_" + nMCHTracks + "MCHTraks_" +  nMuons +  "Mu_" + nPions +"PiPerEvent"
    return load_data_from_file(datafile)

def load_data_from_file(filename):
# Loads trainning data from a file
# inputs: 
#  - filename (without .py extension)
# returns:
#  - pairs_data: Best MFT track ID for each MCH track (-1 => no match)
#  - pairs_match, N_fake_match: absolute number of correct and fake matches
#  - NPairs_MCH_tracks: absolute number of rejected MCH track (no MFT match)
    import importlib
    modulename = 'datetime'
    if "data" in sys.modules:
        del data
    load_start = time.perf_counter()
    print("Loading " +  filename)
    data = importlib.import_module(filename)
    load_finish = time.perf_counter()
    print("Loaded data in ", (load_finish - load_start)/60., " minutes (", load_finish - load_start, " seconds)")
    print("N MCH Tracks = "+str(len(data.track_IDs)))
    print("N pairs = "+str(len(data.pairs_data)))
    return data.pairs_data, data.pairs_match, data.track_IDs

def load_data_from_csv_files(filename_prefix):
# Loads trainning data from a file
# inputs: 
#  - filename (without .py extension)
# returns:
#  - pairs_data: Best MFT track ID for each MCH track (-1 => no match)
#  - pairs_match, N_fake_match: absolute number of correct and fake matches
#  - NPairs_MCH_tracks: absolute number of rejected MCH track (no MFT match)
    print("Loading CSV from", filename_prefix)
    print("Features...")
    features = np.genfromtxt(filename_prefix+'_features'+'.csv',delimiter=',')
    print("Truth array...")
    pairs_truth = np.genfromtxt(filename_prefix+'_pairs_match'+'.csv',delimiter=',')
    print("MCH track ranges...")

    MCHRanges = np.genfromtxt(filename_prefix+'_track_IDs'+'.csv',delimiter=',', dtype = int)
    print("CSV loaded!")
    return features, pairs_truth, MCHRanges

def get_bestMatches(scores, truth, NPairs_MCH_tracks, cut):
# Return array of MFT match ID for each MCH track and totals for correct, fake and no matched tracks 
# inputs: 
#  - scores: ML predicted score
#  - truth: Monte Carlo input truth
#  - NPairs_MCH_tracks: number of input pairs per MCH track on input data
#  - cut: score cut to consider a matches 
# returns:
#  - best_matches: Best MFT track ID for each MCH track (-1 => no match)
#  - N_correct_match: absolute number of correct matches
#  - N_fake_match: absolute number of fake matches
#  - N_rejected_MCH: absolute number of rejected MCH track (no MFT match)
    first_pair = 0
    best_matches = np.array([],dtype=int)
    MCHTrackID=0
    for last_pair in NPairs_MCH_tracks:
        #print("track First = ", first_pair, " ; Last = ", last_pair)
        #print(scores[first_pair:last_pair])
        max_score = np.amax(scores[first_pair:last_pair])
        best_match = np.where(scores[first_pair:last_pair] == max_score)
        if(len(best_match) > 1):
            print("WARNING!!! MCHTrack # ", MCHTrackID, "has", len(best_match), "tracks with score", max_score )
        #print("Best match = ",best_match[0][0], " ; score = ", max_score])
        best_match_ID = best_match[0][0]+first_pair
        if (max_score < cut):
            best_match_ID = -1
        best_matches = np.append(best_matches, best_match_ID)
        first_pair = last_pair;
        MCHTrackID+=1

    N_correct_match = N_fake_match = N_rejected_MCH = 0
    for best_match in best_matches:
        #print(best_match)
        if (best_match < 0):
            N_rejected_MCH+=1
            #print ("  no match")
            continue
        if(truth[best_match]):
            N_correct_match+=1
            
        else:
            N_fake_match+=1
    #print(" Cut = ", cut)
    #print(" Correct Matches = ", N_correct_match)
    #print(" Fake Matches = ", N_fake_match)
    #print(" No Matches = ", N_rejected_MCH)  
    
    return best_matches, N_correct_match, N_fake_match, N_rejected_MCH


def histos_scores(scores, truth, NPairs_MCH_tracks, title):
# Generate histos for scores distributions for correct and fake matches 
# inputs: 
#  - scores: ML predicted score
#  - truth: Monte Carlo input truth
#  - NPairs_MCH_tracks: number of input pairs per MCH track on input data
# returns:

    first_pair = 0
    best_matches = np.array([],dtype=int)
    best_matches_scores = np.array([],dtype=float)
    correct_matches_scores = np.array([],dtype=float)
    fake_matches_scores = np.array([],dtype=float)
    rejected_scores = np.array([],dtype=float)
    MCHTrackID=0
    for last_pair in NPairs_MCH_tracks:
        #print("track First = ", first_pair, " ; Last = ", last_pair)
        #print(scores[first_pair:last_pair])
        max_score = np.amax(scores[first_pair:last_pair])
        best_match = np.where(scores[first_pair:last_pair] == max_score)
        if(len(best_match) > 1):
            print("WARNING!!! MCHTrack # ", MCHTrackID, "has", len(best_match), "tracks with score", max_score )
        #print("Best match = ",best_match[0][0], " ; score = ", max_score)
        best_match_ID = best_match[0][0]+first_pair
        if (max_score < -0.5):
            best_match_ID = -1
        best_matches = np.append(best_matches, best_match_ID)
        best_matches_scores = np.append(best_matches_scores, max_score)
        first_pair = last_pair;
        MCHTrackID+=1


    N_correct_match = N_fake_match = N_rejected_MCH = 0
    ntrk = 0
    for best_match in best_matches:
        #print(best_match)
        if(ntrk >= len(best_matches_scores)-1):
            continue
        best_match_score = best_matches_scores[ntrk]
        ntrk+=1

        if (best_match < 0):
            N_rejected_MCH+=1
            rejected_scores = np.append(rejected_scores, best_match_score)
            #print ("  no match")
            continue
        if(truth[best_match]):
            N_correct_match+=1
            correct_matches_scores = np.append(correct_matches_scores, best_match_score)
        else:
            N_fake_match+=1
            fake_matches_scores = np.append(fake_matches_scores, best_match_score)
    print("Unfiltered performance:", title)
    print(" Correct Matches = ", N_correct_match)
    print(" Fake Matches = ", N_fake_match)
    
    
    fig, ax = plt.subplots()
    ax.set_xlabel('Matching score')
    ax.hist(correct_matches_scores, 140, (-0.2, 1.2), ec='green', density=True, fc='none', lw=1.5, histtype='step', label='correct') 
    ax.hist(fake_matches_scores, 140, (-0.2, 1.2), ec='red', density=True, fc='none', lw=1.5, histtype='step', label='fake') 
    plt.title(title)
    plt.legend()
    plt.show()
    
    return 


def total_and_ratios_plots(scores, truth, NPairs_MCH_tracks, title, showplots = True):
# Function to plot Correct, Fake and rejection rates vs cutting score
# inputs: 
#  - scores: ML predicted score
#  - NPairs_MCH_tracks: number of input pairs per MCH track on input data
    plt.rc('font', size=15)
    matplotlib.rcParams['figure.figsize'] = (13,8)
    cuts = N_correct_matchArr = N_fake_matchArr = N_rejected_MCH_Arr = np.array([])
    for cut in np.arange (0.05,1.01,0.025):
        best_matches, N_correct_match, N_fake_match, N_rejected_MCH = get_bestMatches(scores, truth, NPairs_MCH_tracks,cut)
        cuts = np.append(cuts, cut)
        N_correct_matchArr = np.append(N_correct_matchArr, N_correct_match)
        N_fake_matchArr = np.append(N_fake_matchArr, N_fake_match)
        N_rejected_MCH_Arr = np.append(N_rejected_MCH_Arr, N_rejected_MCH)
    correct_match_ratio = N_correct_matchArr/(N_correct_matchArr+N_fake_matchArr)
    fake_match_ratio = N_fake_matchArr/(N_correct_matchArr+N_fake_matchArr)
    mch_rejection_ratio = N_rejected_MCH_Arr/(N_correct_matchArr+N_fake_matchArr+N_rejected_MCH_Arr)
    
    if (showplots):
        
        histos_scores(scores, truth, NPairs_MCH_tracks, title)
        
        fig, ax = plt.subplots()
        ax.scatter(cuts, N_correct_matchArr, color = "green", label=u"Correct match" )
        ax.scatter(cuts, N_fake_matchArr, color = "red", label=u"Fake Match" )
        ax.scatter(cuts, N_rejected_MCH_Arr, color="blue", label=u"Rejected MCH track" )
        ax.set_xlabel('Score cut')
        ax.set_ylabel('n Tracks')
        plt.title(title)
        plt.legend()

        fig, ax = plt.subplots()
        ax.scatter(cuts, correct_match_ratio, color = "green", label="Correct match ratio" )
        ax.set_xlabel('Score cut')
        ax.set_ylabel('Correct match ratio')
        plt.title(title)
        plt.legend()


        fig, ax = plt.subplots()
        ax.scatter(cuts, fake_match_ratio, color = "red", label="Fake match ratio" )
        ax.set_xlabel('Score cut')
        ax.set_ylabel('Fake match ratio')
        plt.title(title)
        plt.legend()

        fig, ax = plt.subplots()
        ax.scatter(cuts, mch_rejection_ratio, color = "blue", label=u"Rejection ratio" )
        ax.set_xlabel('Score cut')
        ax.set_ylabel('MCH track rejection ratio')
        plt.title(title)
        plt.legend()

        fig, ax = plt.subplots()
        ax.scatter(mch_rejection_ratio,
                   correct_match_ratio, 
                   color = "green", label=u"Correct match ratio" )
        ax.set_xlabel('MCH Track rejection ratio')
        ax.set_xlim([0,1])
        ax.set_ylabel(u'Correct MCH-MFT track match')
        plt.title(title)
        plt.legend()

        fig, ax = plt.subplots()


        ax.scatter(cuts, correct_match_ratio, color = "green", label=u"Correct match ratio" )
        ax.scatter(cuts, fake_match_ratio, color = "red", label=u"Fake match ratio" )
        ax.scatter(cuts, mch_rejection_ratio, color = "blue", label=u"Rejection ratio" )
        plt.title(title)
        plt.legend()    

        plt.show()
        print("cuts: ",cuts)
        print("correct matches : ",N_correct_matchArr)
        print("fake matches: ",N_fake_matchArr)
        print("no matches: ",N_rejected_MCH_Arr)
    return [correct_match_ratio, fake_match_ratio, mch_rejection_ratio]


    
def MLPRegressor_initAndTrain(hidden_layers_cfg_,
                              solver_,
                              max_iter_,
                              activation_,
                              seed,
                              trainning_data_,
                              trainning_data_MatchTruth_):
    regressor_start = time.perf_counter()
    regressor = MLPRegressor(hidden_layer_sizes=hidden_layers_cfg_,
                        solver = solver_,
                        max_iter = max_iter_,
                        activation = activation_,
                        random_state=seed)
    regressor.fit(trainning_data_, trainning_data_MatchTruth_)
    regressor_finish = time.perf_counter()
    print("mlpr trainning time = ", (regressor_finish - regressor_start)/60., " minutes")
    acc = regressor.score(trainning_data_, trainning_data_MatchTruth_)
    print("Trainning Accuracy: ", acc)
    return regressor
    
def GradBoostReg_initAndTrain(n_estimators_,
                              max_depth_,
                              loss_,
                              trainning_data_,
                              trainning_data_MatchTruth_):
    gbr_start = time.perf_counter()
    gbr = GradientBoostingRegressor(n_estimators=n_estimators_, max_depth=max_depth_, loss=loss_)

    #mlpr = MLPRegressor(hidden_layer_sizes=hidden_layers_cfg_,
    #                    solver = solver_,
    #                    max_iter = max_iter_,
    #                    activation = activation_)
    gbr.fit(trainning_data_, trainning_data_MatchTruth_)
    gbr_finish = time.perf_counter()
    print("GBR trainning time = ", (gbr_finish - gbr_start)/60., " minutes")
    acc = gbr.score(trainning_data_, trainning_data_MatchTruth_)
    print("Trainning Accuracy: ", acc)
    return gbr

def AdaBoostReg_initAndTrain(n_estimators_,
                              max_depth_,
                              trainning_data_,
                              trainning_data_MatchTruth_):
    ada_start = time.perf_counter()
    ada = AdaBoostRegressor(DecisionTreeRegressor(max_depth=max_depth_),n_estimators=n_estimators_)

    #mlpr = MLPRegressor(hidden_layer_sizes=hidden_layers_cfg_,
    #                    solver = solver_,
    #                    max_iter = max_iter_,
    #                    activation = activation_)
    ada.fit(trainning_data_, trainning_data_MatchTruth_)
    ada_finish = time.perf_counter()
    print("GBR trainning time = ", (ada_finish - ada_start)/60., " minutes")
    acc = ada.score(trainning_data_, trainning_data_MatchTruth_)
    print("Trainning Accuracy: ", acc)
    return ada


from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import AdaBoostRegressor

def getHidd_Lay_str(cfg):
    lcfg_str='('
    for n in cfg['hidden_layers_cfg']:
        lcfg_str=lcfg_str+str(n)+","
    lcfg_str=lcfg_str[:-1]
    lcfg_str=lcfg_str+")"
    return lcfg_str

def getcfg_str(cfg):
    lcfg_str=getHidd_Lay_str(cfg)
    lcfg_str=lcfg_str+" max_iter: "+str(cfg['max_iter'])
    lcfg_str=lcfg_str+" activation = " + cfg['activation']

    return lcfg_str

def benchmark_MLPRegressor(cfg, trainning_data,
                          trainning_data_MatchTruth,
                          trainning_data_NPairsMCHTracks,
                          test_data,
                          test_data_MatchTruth,
                          test_data_NPairsMCHTracks,
                          showplots_ = True):
    
    config_string = getcfg_str(cfg)
    print("Starting benchmark: "+config_string, "on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))
    print(cfg)
    # Load training data
    #trainning_data, trainning_data_MatchTruth, trainning_data_NPairsMCHTracks = load_data_from_csv_files(cfg['trainning_data_file'])
    
    # Scale training data
    #scaler = StandardScaler()
    #scaler.fit(trainning_data)
    #trainning_data = scaler.transform(trainning_data)
    
    # Init and train neural network
    print("Trainning MLPRegressor on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))
    regressor = MLPRegressor_initAndTrain(cfg['hidden_layers_cfg'],
                              cfg['solver'],
                              cfg['max_iter'],
                              cfg['activation'],
                              cfg['random_seed'],
                              trainning_data_ = trainning_data,
                              trainning_data_MatchTruth_ = trainning_data_MatchTruth
                             )
    
    ## Load test data
    #print("Loading test data...")
    #test_data, test_data_MatchTruth, test_data_NPairsMCHTracks = load_data_from_csv_files(cfg['test_data_file'])
    # Scale test data 
    #print("Scalling test data...")
    #test_data = scaler.transform(test_data)

    # Apply trainning on input data
    print("Running MLPRegressor prediction...")
    Match_pred = regressor.predict(test_data)

    acc2 = regressor.score(test_data, test_data_MatchTruth)
    print("Prediction accuracy:\n", acc2)

    # Get results (plots and trained neural network
    print("Summarizing results on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))
    return [regressor, total_and_ratios_plots(Match_pred,
                           test_data_MatchTruth,
                           test_data_NPairsMCHTracks,
                           config_string + '\n' + cfg['test_data_file'],
                           showplots = showplots_
                           ) ]



def benchmark_GradBoostReg(cfg, trainning_data,
                          trainning_data_MatchTruth,
                          trainning_data_NPairsMCHTracks,
                          test_data,
                          test_data_MatchTruth,
                          test_data_NPairsMCHTracks,
                          showplots_ = True):
    config_string = str(cfg['n_estimators'])+", "+str(cfg['max_depth'])
    print("Starting benchmark: "+config_string, "on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))
    print(cfg)
    # Load trainning data
    trainning_data, trainning_data_MatchTruth, trainning_data_NPairsMCHTracks = load_data_from_csv_files(cfg['trainning_data_file'])
    
    # Scale trainning data
    #scaler = StandardScaler()
    #scaler.fit(trainning_data)
    #trainning_data = scaler.transform(trainning_data)
    
    # Init and train neural network
    print("Starting Trainning Gradient Boosted Regressor on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))

    regressor = GradBoostReg_initAndTrain(cfg['n_estimators'],
                              cfg['max_depth'],
                              cfg['loss'],
                              trainning_data_ = trainning_data,
                              trainning_data_MatchTruth_ = trainning_data_MatchTruth
                             )

    print("Training completed on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))

    ## Load test data
    print("Loading test data...")
    test_data, test_data_MatchTruth, test_data_NPairsMCHTracks = load_data_from_csv_files(cfg['test_data_file'])

    # Scale test data 
    #print("Scalling test data...")
    #test_data = scaler.transform(test_data)

    # Apply trainning on input data
    print("Running Gradient Boosted Regressor prediction...")
    Match_pred = regressor.predict(test_data)

    acc2 = regressor.score(test_data, test_data_MatchTruth)
    print("Prediction accuracy:\n", acc2)

    # Get results (plots and trained neural network
    print("Summarizing results on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))
    return [regressor, total_and_ratios_plots(Match_pred,
                           test_data_MatchTruth,
                           test_data_NPairsMCHTracks,
                           config_string + '\n' + cfg['test_data_file'],
                           showplots = showplots_
                           ) ]

def benchmark_AdaBoostReg(cfg, trainning_data,
                          trainning_data_MatchTruth,
                          trainning_data_NPairsMCHTracks,
                          test_data,
                          test_data_MatchTruth,
                          test_data_NPairsMCHTracks,
                          showplots_ = True):
    config_string = str(cfg['n_estimators'])+", "+str(cfg['max_depth'])
    print("Starting benchmark: "+config_string, "on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))
    print(cfg)
    
    # Init and train neural network
    print("Starting AdaBoost Regressor trainning on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))

    regressor = AdaBoostReg_initAndTrain(cfg['n_estimators'],
                              cfg['max_depth'],
                              trainning_data_ = trainning_data,
                              trainning_data_MatchTruth_ = trainning_data_MatchTruth
                             )

    print("Trainning completed on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))


    # Apply trainning on input data
    print("Running AdaBoost Regressor prediction...")
    Match_pred = regressor.predict(test_data)

    acc2 = regressor.score(test_data, test_data_MatchTruth)
    print("Prediction accuracy:\n", acc2)

    # Get results (plots and trained neural network
    print("Summarizing results on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))
    return [regressor, total_and_ratios_plots(Match_pred,
                           test_data_MatchTruth,
                           test_data_NPairsMCHTracks,
                           config_string + '\n' + cfg['test_data_file'],
                           showplots = showplots_
                           ) ]

def benchmark_AutoML(trainning_data,
                     trainning_data_MatchTruth,
                     trainning_data_NPairsMCHTracks,
                     test_data,
                     test_data_MatchTruth,
                     test_data_NPairsMCHTracks,
                     mode_ = 'Perform',
                     ml_task_ = 'regression',
                     model_time_limit_ = None,
                     showplots_ = True):
    
    print("Starting AutoML Benchmark on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))
    config_string = "Mode: "+ mode_ + " ; ml_task: " +  ml_task_
   
    # Init and train neural network
    print("Trainning AutoMLr on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))
    regressor = automl = AutoML(mode=mode_, ml_task=ml_task_, model_time_limit = model_time_limit_)
    automl.fit(trainning_data, trainning_data_MatchTruth)

    # Apply trainning on input data
    print("Running AutoMLr prediction...")
    Match_pred = regressor.predict(test_data)

    acc2 = regressor.score(test_data, test_data_MatchTruth)
    print("Prediction accuracy:\n", acc2)

    # Get results (plots and trained neural network
    print("Summarizing results on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))
    return [regressor, total_and_ratios_plots(Match_pred,
                           test_data_MatchTruth,
                           test_data_NPairsMCHTracks,
                           config_string,
                           showplots = showplots_
                           ) ]
    

    
def runBenchmarksMLPR(config_list, trainning_data_file, test_data_file):
    print("Starting benchmarks on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))
    benchmark_start = time.perf_counter()

    configurations_strings = []
    trainned_regressors = []
    benchmark_durantions = []
    correct_matches_vectors = []
    fake_matches_vectors = []
    rejection_ratios_vectors = []  
    
    trainning_data, trainning_data_MatchTruth, trainning_data_NPairsMCHTracks = load_data_from_csv_files(trainning_data_file)
    test_data, test_data_MatchTruth, test_data_NPairsMCHTracks = load_data_from_csv_files(test_data_file)
    
    for config in config_list:
        config_string = getcfg_str(config)
        bench_start = time.perf_counter()
        
        benchmark_results = benchmark_MLPRegressor(config,
                                                   trainning_data,
                                                   trainning_data_MatchTruth,
                                                   trainning_data_NPairsMCHTracks,
                                                   test_data,
                                                   test_data_MatchTruth,
                                                   test_data_NPairsMCHTracks)
        configurations_strings.append(config_string)
        trainned_regressors.append(benchmark_results[0])
        elapsed = time.perf_counter() - bench_start
        benchmark_durantions.append(elapsed)
        print("Elapsed time benchmark ("+config_string+") => "+ str(elapsed)+" s")

        correct_match_ratio, fake_match_ratio, mch_rejection_ratio = benchmark_results[1][0], benchmark_results[1][1], benchmark_results[1][2]
        
        correct_matches_vectors.append(correct_match_ratio)
        fake_matches_vectors.append(fake_match_ratio)
        rejection_ratios_vectors.append(mch_rejection_ratio)

    title = "trainning_data: " + config_list[0]['trainning_data_file'] +"\n"+ "test_data: " + config_list[0]['test_data_file']
    fig, ax = plt.subplots()
    ax.set_xlabel('MCH Track rejection ratio')
    ax.set_xlim([0,1])
    ax.set_ylabel(u'Correct MCH-MFT track match')
    plt.title(title)

    for cfgID in range (len(configurations_strings)):
        cfg_str=getHidd_Lay_str(config_list[cfgID])

        ax.scatter(rejection_ratios_vectors[cfgID],
                   correct_matches_vectors[cfgID], 
                   label=cfg_str)
    plt.legend()
    plt.show()


    fig, ax = plt.subplots()
    ax.set_xlabel('MCH Track rejection ratio')
    ax.set_xlim([0,1])
    ax.set_ylim([0.85,1])
    ax.set_ylabel(u'Correct MCH-MFT track match')
    plt.title(title)

    for cfgID in range (len(configurations_strings)):
        cfg_str=getHidd_Lay_str(config_list[cfgID])

        ax.scatter(rejection_ratios_vectors[cfgID],
                   correct_matches_vectors[cfgID], 
                   label=cfg_str)
    plt.legend()
    plt.show()    
    
    fig, ax = plt.subplots()
    ax.set_xlabel('CFG')
    #ax.set_xlim([0,1])
    ax.set_ylabel(u'Total processing time (s)')
    plt.title(title)

    for cfgID in range (len(configurations_strings)):
        cfg_str=getHidd_Lay_str(config_list[cfgID])

        ax.scatter(cfgID,benchmark_durantions[cfgID],
                   label=cfg_str)
    plt.legend()
    plt.show()    
    print("runBenchmarksMLPR completed on on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))
    benchmark_duration = time.perf_counter() - benchmark_start
    print("Total benchmark duration => "+ str(benchmark_duration)+" s (" + str(benchmark_duration/60)+" minutes)")

    return (configurations_strings, trainned_regressors, benchmark_durantions, correct_matches_vectors, fake_matches_vectors, rejection_ratios_vectors)

def runBenchmarksGradBoostReg(config_list, trainning_data_file, test_data_file):
    print("Starting benchmarks on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))
    configurations_strings = []
    trainned_regressors = []
    benchmark_durantions = []
    correct_matches_vectors = []
    fake_matches_vectors = []
    rejection_ratios_vectors = []

    
    trainning_data, trainning_data_MatchTruth, trainning_data_NPairsMCHTracks = load_data_from_csv_files(trainning_data_file)
    test_data, test_data_MatchTruth, test_data_NPairsMCHTracks = load_data_from_csv_files(test_data_file)
    
    
    for config in config_list:
        config_string = str(config['n_estimators'])+","+str(config['max_depth'])
        
        bench_start = time.perf_counter()
        benchmark_results = benchmark_GradBoostReg(config,
                                                   trainning_data,
                                                   trainning_data_MatchTruth,
                                                   trainning_data_NPairsMCHTracks,
                                                   test_data,
                                                   test_data_MatchTruth,
                                                   test_data_NPairsMCHTracks)
        configurations_strings.append(config_string)
        trainned_regressors.append(benchmark_results[0])
        elapsed = time.perf_counter() - bench_start
        benchmark_durantions.append(elapsed)
        print("Elapsed time benchmark ("+config_string+") => "+ str(elapsed)+" s")

        correct_match_ratio, fake_match_ratio, mch_rejection_ratio = benchmark_results[1][0], benchmark_results[1][1], benchmark_results[1][2]
        
        correct_matches_vectors.append(correct_match_ratio.copy())
        fake_matches_vectors.append(fake_match_ratio.copy())
        rejection_ratios_vectors.append(mch_rejection_ratio.copy())

    title = "trainning_data: " + config_list[0]['trainning_data_file'] +"\n"+ "test_data: " + config_list[0]['test_data_file']
    fig, ax = plt.subplots()
    ax.set_xlabel('MCH Track rejection ratio')
    ax.set_xlim([0,1])
    ax.set_ylabel(u'Correct MCH-MFT track match')
    plt.title(title)

    for cfgID in range (len(configurations_strings)):
        cfg_str=str(config_list[cfgID]['n_estimators'])+","+str(config_list[cfgID]['max_depth'])

        ax.scatter(rejection_ratios_vectors[cfgID],
                   correct_matches_vectors[cfgID], 
                   label=cfg_str)
    plt.legend()
    plt.show()


    fig, ax = plt.subplots()
    ax.set_xlabel('MCH Track rejection ratio')
    ax.set_xlim([0,1])
    ax.set_ylim([0.85,1])
    ax.set_ylabel(u'Correct MCH-MFT track match')
    plt.title(title)

    for cfgID in range (len(configurations_strings)):
        cfg_str=str(config_list[cfgID]['n_estimators'])+","+str(config_list[cfgID]['max_depth'])  

        ax.scatter(rejection_ratios_vectors[cfgID],
                   correct_matches_vectors[cfgID], 
                   label=cfg_str)
    plt.legend()
    plt.show()    
    
    fig, ax = plt.subplots()
    ax.set_xlabel('CFG')
    #ax.set_xlim([0,1])
    ax.set_ylabel(u'Total processing time (s)')
    plt.title(title)

    for cfgID in range (len(configurations_strings)):
        cfg_str=str(config_list[cfgID]['n_estimators'])+","+str(config_list[cfgID]['max_depth'])

        ax.scatter(cfgID,benchmark_durantions[cfgID],
                   label=cfg_str)
    plt.legend()
    plt.show()    
    print("runBenchmarksGradBoostReg completed on on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))

    return (configurations_strings, benchmark_durantions, correct_matches_vectors, fake_matches_vectors, rejection_ratios_vectors)

def runBenchmarksAdaBoostReg(config_list, trainning_data_file, test_data_file):
    print("Starting benchmarks on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))
    configurations_strings = []
    trainned_regressors = []
    benchmark_durantions = []
    correct_matches_vectors = []
    fake_matches_vectors = []
    rejection_ratios_vectors = []

    trainning_data, trainning_data_MatchTruth, trainning_data_NPairsMCHTracks = load_data_from_csv_files(trainning_data_file)
    test_data, test_data_MatchTruth, test_data_NPairsMCHTracks = load_data_from_csv_files(test_data_file)

    for config in config_list:
        config_string = str(config['n_estimators'])+","+str(config['max_depth'])
        bench_start = time.perf_counter()
        benchmark_results = benchmark_AdaBoostReg(config,
                                                  trainning_data,
                                                  trainning_data_MatchTruth,
                                                  trainning_data_NPairsMCHTracks,
                                                  test_data,
                                                  test_data_MatchTruth,
                                                  test_data_NPairsMCHTracks)
        configurations_strings.append(config_string)
        trainned_regressors.append(benchmark_results[0])
        elapsed = time.perf_counter() - bench_start
        benchmark_durantions.append(elapsed)
        print("Elapsed time benchmark ("+config_string+") => "+ str(elapsed)+" s")

        correct_match_ratio, fake_match_ratio, mch_rejection_ratio = benchmark_results[1][0], benchmark_results[1][1], benchmark_results[1][2]

        correct_matches_vectors.append(correct_match_ratio.copy())
        fake_matches_vectors.append(fake_match_ratio.copy())
        rejection_ratios_vectors.append(mch_rejection_ratio.copy())

    title = "trainning_data: " + config_list[0]['trainning_data_file'] +"\n"+ "test_data: " + config_list[0]['test_data_file']
    fig, ax = plt.subplots()
    ax.set_xlabel('MCH Track rejection ratio')
    ax.set_xlim([0,1])
    ax.set_ylabel(u'Correct MCH-MFT track match')
    plt.title(title)

    for cfgID in range (len(configurations_strings)):
        cfg_str=str(config_list[cfgID]['n_estimators'])+","+str(config_list[cfgID]['max_depth'])

        ax.scatter(rejection_ratios_vectors[cfgID],
                   correct_matches_vectors[cfgID], 
                   label=cfg_str)
    plt.legend()
    plt.show()


    fig, ax = plt.subplots()
    ax.set_xlabel('MCH Track rejection ratio')
    ax.set_xlim([0,1])
    ax.set_ylim([0.85,1])
    ax.set_ylabel(u'Correct MCH-MFT track match')
    plt.title(title)

    for cfgID in range (len(configurations_strings)):
        cfg_str=str(config_list[cfgID]['n_estimators'])+","+str(config_list[cfgID]['max_depth'])  

        ax.scatter(rejection_ratios_vectors[cfgID],
                   correct_matches_vectors[cfgID], 
                   label=cfg_str)
    plt.legend()
    plt.show()    
    
    fig, ax = plt.subplots()
    ax.set_xlabel('CFG')
    #ax.set_xlim([0,1])
    ax.set_ylabel(u'Total processing time (s)')
    plt.title(title)

    for cfgID in range (len(configurations_strings)):
        cfg_str=str(config_list[cfgID]['n_estimators'])+","+str(config_list[cfgID]['max_depth'])

        ax.scatter(cfgID,benchmark_durantions[cfgID],
                   label=cfg_str)
    plt.legend()
    plt.show()    
    print("runBenchmarksGradBoostReg completed on on", time.strftime("%d %b %Y %H:%M:%S", time.localtime()))

    return (configurations_strings, benchmark_durantions, correct_matches_vectors, fake_matches_vectors, rejection_ratios_vectors)

